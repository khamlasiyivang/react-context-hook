import logo from './logo.svg';
import './App.css';
import Navbar from './Components/Navbar';
import BookList from './Components/Booklist';



function App() {
  return (
    <div className="App">
      <Navbar />
      <BookList />
    </div>
  );
}

export default App;
